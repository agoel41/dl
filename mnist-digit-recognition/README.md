**Problem description:**

Correctly identify digits from a dataset of tens of thousands of handwriten images.

**Dataset:**

- A Dataset of 60k training images and 10k test images. The digits have been size normalized and centered in a fixed-size image.
- http://yann.lecun.com/exdb/mnist/index.html

**mnist-digit-recognition-nn-tf.ipynb**

- Architecture: NN with one hidden layer(128 units) and output layer(10 units)
- Accuracy: 99.6% in 12 epochs
- Libraries: tensorflow, matplotlib
- It also demonstrate the feature of callback function and stops training the model as soon as the accuracy of 99.6% is acheived.