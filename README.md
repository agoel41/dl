# DL

This is the repository for deep learning stuff

## keras-DL-NN
It contains CNN models to classify images into two categories (A vs P)

- Keras-CNN-VGG16-image-recognition.ipynb - Defines one way of fine tuning the VGG16 model to classify images into two categories
- Keras-CNN-VGG16-Fine-Tuned.ipynb - It defines improved way (compared to Keras-CNN-VGG16-image-recognition.ipynb) of fine tuning the VGG16 model to classify images into two categories
- Keras-CNN-image-recognition.ipynb - It contains a custom CNN (basic model with 1 convolution and 2 dense layers) to classify images into 2 categories. 
- VGG16_AP.h5: This is the saved model output from the Keras-CNN-VGG16-Fine-Tuned.ipynb
- kerasReproducibleResults.ipynb
- vgg16_architecture.png: original VGG16 architecture
- vgg16_without_top_layer.png - VGG16 architecture without the top layer (dense layers)

## docker_apps
This project contains image predictor (based on fine tuned VGG16 model) using flask (a microframework for python) and deployed on a docker container. This web application predicts images of AG and PR.

#### List of files
- static: Folder containing the static content such as HTML etc.
- Dockerfile - used to build the docker image
- predict_image_app.py - python program to predict images of AG and PR
- requirements.txt - File used to install python packages in the docker image.
- VGG16_AP.h5 - Pre trained model of the fine tuned VGG16 architecture

#### Build the docker image using Dockerfile
```
docker build -t docker-image-predictor:latest .
```

#### Run the container built on the image created in the above step using the following command
```
sudo docker run -d -p 5000:5000 docker-image-predictor
```

#### Access the image predictor web app using the following URL:
```
http://127.0.0.1:5000/static/predictImageWithVisuals.html
```

## Image filtering.ipynb
Demonstrate the convolution of an image with a filter, followed by application of maxpool layer on the convolved image