import base64
import numpy as np
import io
from PIL import Image
import keras
from keras import backend as k
from keras.models import Sequential
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array
from flask import request
from flask import jsonify
from flask import Flask
import tensorflow as tf

app = Flask(__name__)

def get_model():
    global model # declare the variable with global scope
    model = load_model('../keras-DL-NN/VGG16_AP.h5') # load the presaved model
    #model = load_model('VGG16_cats_and_dogs.h5') # load the presaved mode
    model._make_predict_function()
    print("* Model laoded!")

def preprocess_image(image, target_size):
    if image.mode != "RGB": # Checks if the image is in RGB format and if not then convert it into RGB format
        image = image.connvert("RGB") 
    image = image.resize(target_size) # Resizes the image to the specified target size 
    image = img_to_array(image) # converts the image to the numpy array
    image = np.expand_dims(image, axis=0) # expands the dimension of the image

    return image

print(" * Loading keras model...")
get_model()

global graph
graph = tf.get_default_graph()

@app.route("/predictImage", methods=["POST"])
def predict():
    message = request.get_json(force=True)
    encoded = message['image'] # This is the base64 encoded image sent by the client
    decoded = base64.b64decode(encoded) # decode the image
    image = Image.open(io.BytesIO(decoded)) # opens the image file. We have our image data in memory as bytes not as an actual file.
    processed_image = preprocess_image(image, target_size=(224,224))
    
    #model.predict()
    with graph.as_default():
        prediction = model.predict(processed_image).tolist() # predict the image. tolist() converts a numpy array to python list

    response = {
        'prediction' : {
            'ankur': prediction[0][0],
            'priyanka': prediction[0][1]
        }
    }

    return jsonify(response)