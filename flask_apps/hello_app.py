from flask import request
from flask import jsonify
from flask import Flask

# Creates the instance of the Flask class. 
# The name argument with the double underscores on each side is the name of the applications module
app = Flask(__name__)

# This tells what URL the user has to browse to, in order for the function underneath to be called. 
# This is also called the endpoint. The methods parameter specifies what kind of http request are allowed for this endpoint
@app.route('/hello', methods=['post'])

def hello():
    # force=True, tells Flask to always try to parse json from the request even if its unsure of the datatype
    message = request.get_json(force=True)
    name = message['name']
    response = {
           'greeting': 'Hello, ' + name + '!'
    }
    return jsonify(response)