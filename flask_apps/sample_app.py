# Import the Flask class
from flask import Flask

# Creates the instance of the Flask class. 
# The name argument with the double underscores on each side is the name of the applications module
app = Flask(__name__)

# This tells what URL the user has to browse to, in order for the function underneath to be called. 
# This is also called the endpoint.
@app.route('/sample')

def running():
    return 'Flask is running!'